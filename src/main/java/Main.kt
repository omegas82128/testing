fun isEven(number:Int):Boolean{
    return  number%2 == 0
}

fun isPrime(number: Int):Boolean{
    var divisor = 2
    while (divisor <= number / 2) {
        if (number % divisor == 0) {
            return false
        }
        ++divisor
    }
    return true
}

fun main() {
    val number = 5
    print("$number is "+
            if (isEven(5)){
                "even"
            }else {
                "odd"
            }
    )
    println(" and it is "+
            if (!isPrime(5)){
                "not "
            }else{
                ""
            }+"prime number"
    )

}