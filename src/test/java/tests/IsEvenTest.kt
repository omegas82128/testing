package tests

import isEven
import org.junit.Assert
import org.junit.Test

class IsEvenTest {

    @Test
    fun testOdd() {
        Assert.assertFalse(isEven(ODD_NUMBER))
    }

    @Test
    fun testEven() {
        Assert.assertTrue(isEven(EVEN_NUMBER))
    }
    companion object {
        private const val ODD_NUMBER = 3
        private const val EVEN_NUMBER = 2
    }
}