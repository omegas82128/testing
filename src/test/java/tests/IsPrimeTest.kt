package tests

import isPrime
import org.junit.Assert
import org.junit.Test

class IsPrimeTest {

    @Test
    fun testPrimeOne() {
        Assert.assertTrue(isPrime(NUMBER_ONE))
    }
    @Test
    fun testPrimeTwo() {
        Assert.assertTrue(isPrime(NUMBER_TWO))
    }
    @Test
    fun testPrimeThree() {
        Assert.assertFalse(isPrime(NUMBER_THREE))
    }
    companion object {
        private const val NUMBER_ONE = 5
        private const val NUMBER_TWO = 2
        private const val NUMBER_THREE = 22
    }
}