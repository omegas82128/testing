package suites

import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses
import tests.IsEvenTest
import tests.IsPrimeTest

//JUnit Suite Test
@RunWith(Suite::class)
@SuiteClasses(IsEvenTest::class, IsPrimeTest::class)
class MainTestSuite