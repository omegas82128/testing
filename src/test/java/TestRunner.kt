import org.junit.runner.JUnitCore
import tests.IsEvenTest

object TestRunner {
    @JvmStatic
    fun main(args: Array<String>) {
        val result = JUnitCore.runClasses(IsEvenTest::class.java)
        for (failure in result.failures) {
            println("Test failed: $failure")
        }
        if (result.wasSuccessful()){
            println("Tests were successful")
        }
    }
}